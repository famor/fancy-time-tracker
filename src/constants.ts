const AppName = "Fancy Time Tracker"

const DBVersion = 1;
const DBName = "FancyTimeTracker";

const AirtableTableName = "FancyTimeTracker"
const AirtableSecretLength = 17

export {
  AppName,
  DBVersion,
  DBName,
  AirtableTableName,
  AirtableSecretLength,
}
