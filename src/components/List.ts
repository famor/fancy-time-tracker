import m, { Children } from "mithril";


interface ListItemAttrs {
  title: Children;
  detail: Children;
  right?: Children;
}

interface ListAttrs {
  children: Children;
}

const ListItem: m.Component<ListItemAttrs> = {
  view: ({ attrs: { title, detail, right } }) =>
    m(".list-item", [
      m(".item-content", [
        m(".item-title", title),
        m("small.item-detail", detail)
      ]),
      right
    ])
}

const List: m.Component<ListAttrs> = {
  view: ({ children }) => m(".list", children)
}

export { List, ListItem };
