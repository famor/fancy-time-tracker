import m from "mithril";

interface TopBarAttrs {
  title: string;
  date: string;
  // backLink?: string
}

const TopBar: m.Component<TopBarAttrs> = {
  view: ({ attrs: { title, date } }) =>
    m(".topbar", [
      m(".title h3", [
        title,
        m("small.ml-2", date),
      ]),
    ])
}

export default TopBar
