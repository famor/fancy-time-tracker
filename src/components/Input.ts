import m from "mithril";


interface InputAttrs {
  id?: string;
  type?: string;
  value?: string;
  placeholder: string;
  hint?: string;
  isSuccess?: boolean;
  isError?: boolean;
  size?: number;
  oninput?: (event: { target: HTMLInputElement }) => void;
  onchange?: (event: { target: HTMLInputElement }) => void;
  minlength?: Date | string | number;
  maxlength?: Date | string | number;
  pattern?: string;
  disabled?: boolean;
}

interface LabelInputAttrs extends InputAttrs {
  label: string;
}

const Input: m.Component<InputAttrs> = {
  view: ({
           attrs: {
             id,
             type = "text",
             placeholder,
             hint,
             isSuccess = false,
             isError = false,
             size,
             minlength,
             maxlength,
             oninput,
             onchange,
             pattern,
             value = "",
             disabled = false
           }
         }) => {
    const state = isSuccess ? "is-success" : isError ? "is-error" : "";

    return m(`input.form-input ${state}`,
      {
        type,
        id,
        placeholder,
        minlength,
        maxlength,
        oninput,
        onchange,
        pattern,
        value,
        disabled,
        size
      },
      (isSuccess || isError) && hint && m("p.form-input-hint", hint)
    );
  }
};

const LabelInput: m.Component<LabelInputAttrs> = {
  view: ({ attrs }) => {
    const id = attrs.label.replace(" ", "-");

    return m(".form-group", [
      m(".col-6 ", [
        attrs.label && m("label.form-label", {
            for: id
          },
          attrs.label
        )
      ]),
      m(".col-6 ", [
        m(Input, {
          ...attrs,
          id
        })
      ])
    ]);
  }
};

export { Input, LabelInput };
