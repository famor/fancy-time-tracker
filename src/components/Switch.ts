import m from "mithril";

interface SwitchAttrs {
  label: string;
  checked: boolean;
  disabled?: boolean;
  loading?: boolean;
  onchange: (event: { target: HTMLInputElement }) => void;
}

const Switch: m.Component<SwitchAttrs> = {
  view: ({
           attrs: {
             label,
             checked,
             onchange,
             loading,
             disabled
           }
         }) =>
    m(".form-group", [
      m("label.form-switch", {
        class: loading ? "loading" : ""
      }, [
        m("input", {
          type: "checkbox",
          checked,
          onchange,
          disabled
        }),
        m("i.form-icon"),
        label
      ])
    ])
};

export default Switch;
