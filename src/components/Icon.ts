import m from "mithril";


interface IconAttrs {
  icon: // Navigation icons
    | "arrow-up"
    | "arrow-right"
    | "arrow-down"
    | "arrow-left"
    | "upward"
    | "forward"
    | "downward"
    | "back"
    | "caret"
    | "menu"
    | "apps"
    | "more-horiz"
    | "more-vert"
    // Action icons
    | "resize-horiz"
    | "resize-vert"
    | "plus"
    | "minus"
    | "cross"
    | "check"
    | "stop"
    | "shutdown"
    | "refresh"
    | "search"
    | "flag"
    | "bookmark"
    | "edit"
    | "delete"
    | "share"
    | "download"
    | "upload"
    | "copy"
    // Object icons
    | "mail"
    | "people"
    | "message"
    | "photo"
    | "time"
    | "location"
    | "link"
    | "emoji";
  form?: boolean;
  large?: boolean;
}

const Icon: m.Component<IconAttrs> = {
  view: ({ attrs: { icon, form, large } }) =>
    m(
      `i.${form ? "form-icon." : ""}icon${
        large ? " icon-2x " : " "
      }icon-${icon}`
    )
};

export default Icon
