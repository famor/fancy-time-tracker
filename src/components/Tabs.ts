import m, { Children } from "mithril";

import pages from "../pages";


interface TabAttrs {
  klass: string;
  route: string;
  title: Children;
}

const Tab: m.Component<TabAttrs> = {
  view: ({ attrs: { title, route, klass } }) =>
    m("li.tab-item", { class: klass },
      m("a", {
          class: klass,
          onclick: () => m.route.set(route)
        },
        title
      )
    )
};


const Tabs: m.Component = {
  view: ({ attrs: {} }) =>
    m("ul.tab", pages.map(tab => {
      const klass = tab.route === m.route.get() ? "active " + tab.klass : tab.klass
        return m(Tab, { ...tab, klass });
      })
    )
};

export default Tabs;
