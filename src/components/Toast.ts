import m, { Vnode } from "mithril";


interface ToastAttrs {
  visible?: boolean
  toast?: "primary" | "success" | "warning" | "error"
  closable?: boolean
  message: string
  onclose?: () => void
}

const Toast: m.Component<ToastAttrs> = {
    view: ({ attrs: {
      visible,
      toast,
      message,
      closable = false,
      onclose,
    } }) =>
      visible && m(`.toast ${toast ? "toast-" + toast : ""} d-inline-block mb-2`, [
        closable && m("button.btn btn-clear float-right", {
          onclick: (event: Event) => {
            event.preventDefault()
            onclose?.()
          }
        })
      ], message)
  };


export default Toast
