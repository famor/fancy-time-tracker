import m from "mithril";
import { AppName } from "../constants";


const Footer: m.Component = {
  view: () =>
    m(
      ".footer",
      m("p", AppName)
    )
};

export default Footer
