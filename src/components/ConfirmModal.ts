import m from "mithril";


interface ConfirmModalAttrs {
  active: boolean
  title: string;
  message?: string;
  confirmLabel?: string;
  cancelLabel?: string;
  confirm: () => void;
  cancel: () => void;
}

const ConfirmModal: m.Component<ConfirmModalAttrs> = {
  view: ({
           attrs: {
             active= false,
             title = "Confirm",
             message = "",
             confirmLabel = "Ok",
             cancelLabel = "Cancel",
             confirm,
             cancel
           }
         }) =>
    m(".modal modal-sm", {class: active ? "active" : ""}, [
      m(m.route.Link, {
        href: "#close",
        class: "modal-overlay",
        "aria-label": "close",
        onclick: cancel
      }),
      m(".modal-container", { role: "document" }, [
        m(".modal-header", [
          m(m.route.Link, {
            href: "#close",
            class: "btn btn-clear float-right",
            "aria-label": "close",
            onclick: cancel
          }),
          m(".modal-title h5", title)
        ]),
        m(".modal-body", [
          m(".content", [
            m("p", message)
          ])
        ]),
        m(".modal-footer", [
          m("button.btn btn-error", {
            onclick: confirm
          }, confirmLabel),
          m(m.route.Link, {
            href: "#cancel",
            class: "btn btn-link",
            "aria-label": cancelLabel,
            onclick: cancel
          }, cancelLabel)
        ])
      ])
    ])
};

export { ConfirmModal };
