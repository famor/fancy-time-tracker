import m, { Children } from "mithril";
import Icon from "./Icon";


export interface EmptyAttrs {
  content: Children;
  action?: Children;
}

const Empty: m.Component<EmptyAttrs> = {
  view: ({ attrs: { content, action } }) =>
    m(".empty", [
      m("p.empty-title h4", content),
      action ? m(".empty-action", action) : null
    ])
};

const Empty404: m.Component = {
  view: () =>
    m(Empty, {
      content: "Page not found",
      action: m(
        m.route.Link,
        { href: "/" },
        m(Icon, { icon: "back" }),
        " Return to app"
      )
    })
}

export { Empty, Empty404 };
