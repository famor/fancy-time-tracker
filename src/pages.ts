import m, { Children } from "mithril";

import { Empty } from "./components/Empty";
import { TimeLogView } from "./views/TimeLogView";
import PreferencesView from "./views/PreferencesView";

type page = {
  title: Children,
  route: string,
  klass: string,
  component: m.Component<any>
}

const pages: page[]  = [
  {
    title: "Today",
    route: "/",
    klass: "",
    component: TimeLogView,
  },
  {
    title: "Week",
    route: "/week",
    klass: "",
    component: Empty,
  },
  {
    title: "Month",
    route: "/month",
    klass: "",
    component: Empty,
  },
  {
    title: "Preferences",
    route: "/preferences",
    klass: "tab-preferences",
    component: PreferencesView,
  }
];

export default pages
export { type page }
