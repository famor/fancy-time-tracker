import m from "mithril";
import { AirtableTableName } from "../constants";
import TimeLog from "./TimeLog";


declare const DEV: boolean;

interface RequestError extends Error {
  code: number
  response: FlakyAirtableError | AirtableError
}

interface FlakyAirtableError extends RequestError {
  code: number
  error: "NOT_FOUND" | string,
}

interface AirtableError extends RequestError {
  code: number
  error: {
    type: "TABLE_NOT_FOUND" | string,
    message: string,
  };
}

interface AirtableRecords<T> {
  records: AirtableRecord<T>[];
}
interface AirtableRecord<T> {
  id: string;
  fields: T;
  createdTime?: string
}

interface AirtableRecordsPOST<T> {
  records: AirtableRecordPOST<T>[];
}

interface AirtableRecordPOST<T> {
  fields: T;
}
interface AirtableRecordsDeleted {
  records: AirtableRecordDeleted[]
}
interface AirtableRecordDeleted {
  id: string;
  deleted: boolean
}

type ApiResponse<T> = Promise<AirtableRecords<T> | AirtableRecord<T> | AirtableError>

const handleHTTPError = (error: AirtableError) => {
  if (DEV) console.log("handleHTTPError", error)
  switch (error.code) {
    case 400:
    case 401:
    case 402:
    case 403:
    case 404:
      switch (error.error.type) {
        case "TABLE_NOT_FOUND":
          if (DEV) console.log("Table not found");
          break;
        case "NOT_FOUND":
          if (DEV) console.log("Base not found or apiKey not correctly");
      }
    case 413:
    case 422:
      break;
    case 500:
    case 502:
    case 503:
      if (DEV) console.log("Airtable server error", error);
      break;
  }
  return error
}

type Request = {
  method?: "GET" | "POST" | "PUT" | "PATCH" | "DELETE" | "HEAD" | "OPTIONS"
  path?: string | undefined
  body?: any | undefined
  params?: { [key: string]: any } | undefined
}

export type AirtableType = {
  enabled: boolean
  apiKey: string
  base?: string
  table: string
  baseUrl: string
  _request: <T>(request: Request) => ApiResponse<T>
  GET: (recordID?: string, params?: { [key: string]: any } | undefined) => ApiResponse<TimeLog>
  POST: (body: AirtableRecordsPOST<TimeLog>) => ApiResponse<TimeLog>
  PUT: (body: AirtableRecords<TimeLog>) => ApiResponse<TimeLog>
  DELETE: (recordID: string | string[]) => ApiResponse<AirtableRecordsDeleted | AirtableRecordDeleted>
  test: () => ApiResponse<TimeLog>
  newTimeLog: (timeLog: TimeLog) => Promise<boolean>
}

const Airtable = function(apiKey: string, base: string): AirtableType {
  return {
    enabled: true,
    apiKey: apiKey ?? "",
    base: base ?? "",
    table: AirtableTableName,
    baseUrl: "https://api.airtable.com/v0",
    _request<T>(
      {
        method = "GET",
        path = "",
        body = undefined,
        params = undefined
      }: Request): ApiResponse<T> {
      if (!this.enabled) {
        if (DEV) console.log("Airtable is disabled, shouldn't be doing this.")
        throw {
          code: 1,
          error: {
            type: "AIRTABLE_DISABLED",
            message: "Airtable is disabled, can't make requests.",
          }
        }
      }
      return m.request<ApiResponse<T>>({
          method,
          url: `${this.baseUrl}/${this.base}/${this.table}${path}`,
          body,
          params,
          headers: { Authorization: `Bearer ${this.apiKey}` }
        })
        .then((result) => {
          return result;
        })
        .catch((error: RequestError) => {
          if (DEV) console.log("RequestError", error.code, error.response)
          if (error.code === 0 || error.code < 100) {
            if (DEV) console.log("Network unavailable", error.code);
            throw {
              code: error.code,
              error: {
                type: "ERR_INTERNET_DISCONNECTED",
                message: "Working offline. Check your connection and try again."
              }
            } as AirtableError;
          }
          if (typeof error.response.error === "string") {
            if (DEV) console.log("Unexpected error", error.code, error.response);
            throw {
              code: error.code,
              error: {
                type: error.response.error,
                message: "Couldn't enable Airtable. Did you paste the correct apiKey and Base values?"
              }
            } as AirtableError;
          }
          throw error.response
        })
        .catch((error: AirtableError) => {
          if (DEV) console.log("AirtableError", error)
          return handleHTTPError(error)
        })
    },
    GET(recordID?: string): ApiResponse<TimeLog> {
      if (recordID) return this._request<TimeLog>({ path: recordID })
      return this._request<TimeLog>({});
    },
    POST(body: AirtableRecordsPOST<TimeLog>): ApiResponse<TimeLog> {
      return this._request<TimeLog>({ method: "POST", body});
    },
    PUT(body: AirtableRecords<TimeLog>): ApiResponse<TimeLog> {
      return this._request<TimeLog>({ method: "PUT", body});
    },
    DELETE(recordIDs: string | string[]): ApiResponse<AirtableRecordsDeleted | AirtableRecordDeleted> {
      if (typeof recordIDs === "string") return this._request<AirtableRecordDeleted>({ path: recordIDs })
      return this._request<AirtableRecordsDeleted>({method: "DELETE", params: {"records[]": recordIDs}});
    },
    test: async function(): ApiResponse<TimeLog> {
      return await this.GET();
    },
    newTimeLog: async function(timeLog: TimeLog): Promise<boolean> {
      const result = await this.POST({
        records: [{ fields: timeLog }]
      }) as AirtableRecords<TimeLog>
      return !("error" in result);
    },
  };
};

export default Airtable;
