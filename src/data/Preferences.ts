import { DateTime } from "luxon";
import { DBVersion } from "../constants";


class Preferences {
  id?: number
  dbVersion: number
  timezone: string
  integrations: {
    airtableEnabled: boolean
    airtableKey: string
    airtableBase: string
    airtablePreferencesID: string
  }

  constructor(
    dbVersion: number,
    timezone: string,
    integrations: {
      airtableEnabled: false,
      airtableKey: "",
      airtableBase: "",
      airtablePreferencesID: "",
    },
    id?: number
  ) {
    this.dbVersion = dbVersion
    this.timezone = timezone
    this.integrations = integrations
    if (id) this.id = id
  }
}


function createPrefs(): Preferences {
  return new Preferences(
    DBVersion,
    DateTime.now().zoneName,
    {
      airtableEnabled: false,
      airtableKey: "",
      airtableBase: "",
      airtablePreferencesID: "",
    },
    1
  );
}

export default Preferences
export { createPrefs }
