import m from "mithril";
import { DexieError, Subscription } from "dexie";
import { DateTime, Settings as LuxonSettings } from "luxon";

import TimeTrackerDB from "./storage/idb";
import TimeLog from "./TimeLog";
import Preferences from "./Preferences";
import { DBName } from "../constants";
import Airtable, { type AirtableType } from "./airtable";


declare const DEV: boolean;

interface StateType {
  loadingStarted: boolean;
  loaded: boolean;
  db?: TimeTrackerDB;
  preferences?: Preferences;
  intervalID?: NodeJS.Timer;
  timeLogSubscription?: Subscription;
  date: string;
  timeLog: TimeLog[];
  airtable?: AirtableType
  setAirtableEnabled: (enabled: boolean) => void
  setAirtableKey: (key: string) => void
  setAirtableBase: (baseId: string) => void
  setAirtableTable: (tableId: string) => void
  setAirtablePreferencesID: (recordID: string) => void
  setTimezone: (timezone: string) => void;
  start: (note?: string, time?: DateTime) => void;
  stop: (time?: DateTime) => void;
  startInterval: () => void;
  stopInterval: () => void;
  started: () => boolean;
  getNow: (timezone?: string) => DateTime;
  isLastStarted: () => boolean;
  updateTimeLogSubscription: () => void;
  init: () => Promise<void>;
  initAirtable: () => Promise<boolean>;
  deleteAll: () => void
}

const setFirstMillisecond = (time: DateTime) => time.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
const setLastMillisecond = (time: DateTime) => time.set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

const State: StateType = {
  loadingStarted: false,
  loaded: false,
  db: undefined,
  preferences: undefined,
  date: DateTime.now().toISODate(),
  timeLog: [],
  intervalID: undefined,
  timeLogSubscription: undefined,
  airtable: undefined,
  setAirtableEnabled (enabled: boolean) {
    if (this.airtable) this.airtable.enabled = enabled
    this.db?.preferences.update(this.preferences!, { "integrations.airtableEnabled": enabled });
  },
  setAirtableKey (key: string) {
    this.db?.preferences.update(this.preferences!, { "integrations.airtableKey": key });
  },
  setAirtableBase (baseId: string) {
    this.db?.preferences.update(this.preferences!, { "integrations.airtableBase": baseId });
  },
  setAirtableTable (tableId: string) {
    this.db?.preferences.update(this.preferences!, { "integrations.airtableTable": tableId });
  },
  setAirtablePreferencesID (recordID: string) {
    this.db?.preferences.update(this.preferences!, { "integrations.airtablePreferencesID": recordID });
  },
  setTimezone(timezone: string) {
    if (timezone.startsWith("System")) timezone = "system"
    this.db?.preferences.update(this.preferences!, { timezone });
  },
  getNow() {
    return DateTime.now().set({millisecond: 0})
  },
  started() {
    return this.timeLog.length > 0 && this.timeLog[this.timeLog.length - 1].started;
  },
  start(note, time) {
    const timeLog = new TimeLog();
    timeLog.note = note ?? timeLog.note;
    timeLog.time = time?.toJSDate() ?? timeLog.time;
    this.db!.timeLog.add(timeLog);

    this.startInterval();
  },
  stop(time) {
    this.stopInterval();

    const timeLog = new TimeLog();
    timeLog.time = time?.toJSDate() ?? timeLog.time;
    timeLog.started = false;
    this.db!.timeLog.add(timeLog);
  },
  startInterval() {
    this.intervalID = this.intervalID ?? setInterval(m.redraw, 200);
  },
  stopInterval() {
    this.intervalID && clearInterval(this.intervalID);
    this.intervalID = undefined;
  },
  isLastStarted() {
    return this.timeLog[this.timeLog.length - 1]?.started;
  },
  updateTimeLogSubscription: async function() {
    if (this.timeLogSubscription) {
      await this.timeLogSubscription.unsubscribe();
    }
    if (this.db === undefined) {
      return
    }
    this.timeLogSubscription = this.db.observeTimeLog(this.date)
      .subscribe({
        next: (result) => {
          this.timeLog = result;
          if (this.isLastStarted()) this.startInterval();
          m.redraw();
        },
        error: (error: DexieError) => {
          if(DEV) console.log("timeLogSubscription", error);
        }
      });
  },
  init: async function() {
    return new Promise(resolve => {
      if (this.loaded || this.loadingStarted) {
        if (DEV) console.log("Init skipped");
        resolve();
        return
      }
      if (DEV) console.time("init");
      this.loadingStarted = true;
      const loading = setInterval(() => {
        if (
          this.db !== undefined
          && this.preferences !== undefined
        ) {
          if (this.timeLogSubscription === undefined) this.updateTimeLogSubscription();
          if (this.airtable === undefined) this.initAirtable()
          this.loaded = true;
          if (DEV) console.timeEnd("init");
          clearInterval(loading);
          resolve();
        }
      }, 5)

      this.db = new TimeTrackerDB();
      this.db.observePreferences().subscribe({
        next: (preferences) => {
          if (!preferences) return
          if (DEV) console.log("Got new preferences", preferences)
          this.preferences = preferences;
          LuxonSettings.defaultZone = preferences.timezone
          this.date = this.getNow().toISODate()
          m.redraw();
        },
        error: (error: DexieError) => {
          if (DEV) console.log("observePreferences", error);
        }
      });
      this.db.preferences.hook("updating", function(modifications, primKey, obj, transaction) {
        this.onsuccess = () => State.updateTimeLogSubscription()
        return modifications
      });
      this.db.timeLog.hook("creating", function(primKey, obj, transaction) {
          if (State.airtable && State.airtable.enabled)
            State.airtable.newTimeLog(obj).then(() => {})
      });

      setInterval(async () => {
        const newDate = this.getNow().toISODate();
        if (this.date !== newDate) {
          if (DEV) console.log("it's a new day!", "old", this.date, "new", newDate);
          this.date = newDate
          await this.updateTimeLogSubscription();
        }
        m.redraw();
      }, 2000);
    })
  },
  initAirtable: async function(): Promise<boolean> {
    if (this.preferences && !this.preferences.integrations.airtableEnabled) {
      if (DEV) console.log("Airtable is disabled")
      return false
    }
    if (
      !this.preferences
      || !this.preferences.integrations.airtableKey
      || !this.preferences.integrations.airtableBase
    ) return false
    this.airtable = Airtable(this.preferences.integrations.airtableKey, this.preferences.integrations.airtableBase)
    const test = await this.airtable.test()
    if ("error" in test) {
      //TODO: show a message to user
      if (DEV) console.log("Can't connect to Airtable", test)
      this.setAirtableEnabled(false);
      return false
    }
    return true
  },
  deleteAll: async function() {
    window.indexedDB.deleteDatabase(DBName);
    m.route.set("/")
  },
};

export default State;
export { setFirstMillisecond, setLastMillisecond };
