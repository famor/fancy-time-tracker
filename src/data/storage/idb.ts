import Dexie, { liveQuery, Observable } from "dexie";
import { DateTime } from "luxon";

import TimeLog from "../TimeLog";
import Preferences, { createPrefs } from "../Preferences";
import { DBVersion, DBName } from "../../constants";

declare const DEV: boolean;


class TimeTrackerDB extends Dexie {
  timeLog!: Dexie.Table<TimeLog, number>;
  preferences!: Dexie.Table<Preferences, number>;

  constructor() {
    super(DBName);

    this.version(DBVersion).stores({
      timeLog: "++id, time, started",
      preferences: "id"
    });
    this.timeLog.mapToClass(TimeLog);
    this.preferences.mapToClass(Preferences);

    this.preferences.count(count => {
      if (count === 0) {
        this.preferences.add(createPrefs());
      }
    });
  }

  observeTimeLog(date: string): Observable<TimeLog[]> {
    return liveQuery(() => {
      const midnightToday = DateTime.fromISO(date);
      const midnightTomorrow = midnightToday.plus({ day: 1 });
      if (DEV) console.log("observeTimeLog", midnightToday.toJSDate(), " - ", midnightTomorrow.toJSDate());
      return this.timeLog
        .where("time")
        .between(midnightToday.toJSDate(), midnightTomorrow.toJSDate(), true, false)
        .toArray();
    });
  }

  observePreferences(): Observable<Preferences> {
    return liveQuery(async () => {
      const preferences = await this.preferences.get(1);
      return preferences ?? createPrefs();
    });
  }
}

export default TimeTrackerDB;
export { DBVersion };
