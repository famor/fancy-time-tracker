import State from "./State";

class TimeLog {
  id?: number
  time: Date
  started: boolean
  note: string

  constructor(
    time: Date = State.getNow().toJSDate(),
    started = true,
    note = "",
    id?:number
  ) {
    this.time = time
    this.started = started
    this.note = note
    if (id) this.id = id
  }
}

export default TimeLog
