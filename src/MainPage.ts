import m from "mithril";
import { DateTime } from "luxon";

import State  from "./data/State";
import Footer from "./components/Footer";
import TopBar from "./components/TopBar";
import Tabs from "./components/Tabs";
import { TimeView } from "./views/TimeView";
import { page } from "./pages";
import { AppName } from "./constants";


interface TodayPageAttrs {
  page: page
}

const MainPage: m.Component<TodayPageAttrs> = {
  oninit: ({attrs: { page }}) => (document.title = `${AppName} - ${page.title}`),
  view: ({attrs: { page }}) => {
    const now = State.getNow()
    return [
      m(TopBar, {
        title: "Today",
        date: now.toLocaleString({ ...DateTime.DATETIME_MED, timeZoneName: "short" })
      }),
      m(".divider"),
      m(TimeView, {now}),
      m(".divider"),
      m(Tabs),
      m(page.component, {now}),
      m(Footer)
    ]
  }
}

export default MainPage;
