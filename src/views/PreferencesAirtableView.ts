import m, { Vnode } from "mithril";

import State from "../data/State";
import Switch from "../components/Switch";
import { LabelInput } from "../components/Input";
import Airtable from "../data/airtable";
import Toast from "../components/Toast";
import { AirtableSecretLength } from "../constants";


function PreferencesAirtableView(): m.Component {
  let loading = false;
  let isSuccess = false
  let isError = false
  let toastMessage = ""

  let airtableEnabled = State.preferences!.integrations.airtableEnabled;
  let airtableKey = State.preferences!.integrations.airtableKey;
  let airtableBase = State.preferences!.integrations.airtableBase;
  let airtableKeyValid = airtableKey.length === AirtableSecretLength;
  let airtableBaseValid = airtableKey.length === AirtableSecretLength;

  const isSwitchDisabled = () =>
    !(airtableKeyValid && airtableKey.length === AirtableSecretLength)
    || !(airtableBaseValid && airtableBase.length === AirtableSecretLength)


  async function enableAirtable(enabled: boolean) {
    isSuccess = false
    isError = false
    toastMessage = ""
    airtableEnabled = enabled
    if (enabled) {
      loading = true;
      isError = false
      let client = Airtable(airtableKey, airtableBase)
      const test = await client.test()
      if ("error" in test) {
        airtableEnabled = false
        toastMessage = test.error.message
        isError = true
      } else {
        isError = false
        await State.setAirtableEnabled(airtableEnabled);
        //  TODO: notify and ask confirmation for possible issues
        if (airtableEnabled) airtableEnabled = await State.initAirtable()
        if (airtableEnabled) {
          isSuccess = true
          toastMessage = "Airtable sync enabled!"
        } else {
          isError = true
          toastMessage = "Unexpected error, check you Airtable Table configuration and try again!"
        }
      }
    } else {
      await State.setAirtableEnabled(airtableEnabled);
    }
    loading = false;
  }

  function keyChanged(key: string, isValid: boolean) {
    airtableKey = key;
    airtableKeyValid = isValid;
    State.setAirtableKey(isValid ? key : "");

  }

  function baseChanged(base: string, isValid: boolean) {
    airtableBase = base;
    airtableBaseValid = isValid;
    State.setAirtableBase(isValid ? base : "");
  }

  return {
    view: () => {
      return m("fieldset", [
        m("legend.mt-2", "Airtable"),
        m(Toast, {
          toast: isSuccess ? "success" : "error",
          closable: true,
          visible: isError || isSuccess,
          message: toastMessage,
          onclose: () => {
            isError = false;
            isSuccess = false
          },
        }),
        m(Switch, {
          checked: airtableEnabled,
          label: "Enable Airtable sync",
          loading,
          disabled: isSwitchDisabled(),
          onchange: (event: { target: HTMLInputElement }) => enableAirtable(event.target.checked)
        }),

        m(LabelInput, {
          type: "password",
          value: airtableKey,
          label: "Airtable api key",
          placeholder: "key..............",
          hint: isSuccess ? undefined : "Did you paste the correct value?",
          isError: !airtableKeyValid || isError,
          isSuccess: airtableKeyValid && !isError && isSuccess,
          oninput: (event: { target: HTMLInputElement }) => keyChanged(event.target.value, event.target.validity.valid),
          minlength: AirtableSecretLength,
          maxlength: AirtableSecretLength,
          pattern: "^key[A-Za-z0-9]{14}",
          disabled: airtableEnabled
        }),
        m(LabelInput, {
          value: airtableBase,
          label: "Airtable base id",
          placeholder: "app..............",
          hint: isSuccess ? undefined : "Did you paste the correct value?",
          isError: !airtableBaseValid || isError,
          isSuccess: airtableBaseValid && !isError && isSuccess,
          oninput: (event: { target: HTMLInputElement }) => baseChanged(event.target.value, event.target.validity.valid),
          minlength: AirtableSecretLength,
          maxlength: AirtableSecretLength,
          pattern: "^app[A-Za-z0-9]{14}",
          disabled: airtableEnabled
        })
        //  TODO: Save / delete secrets to Airtable - MultiSelect
        //  TODO: Clear remote data
      ]);
    }
  };
}

export default PreferencesAirtableView;
