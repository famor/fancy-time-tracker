import m from "mithril";
import { DateTime } from "luxon";

import { List, ListItem } from "../components/List";
import State from "../data/State";


const redCircle = "🔴 "
const greenCircle = "🟢 "

const startStop = false;

interface TimeLogViewAttrs {
  now: DateTime
}

const TimeLogView: m.Component<TimeLogViewAttrs> = {
  view: ({ attrs: { now } }) =>
    m(
      List, [
        startStop && State.timeLog.slice(0).reverse().map(item => {
          const circle = item.started ? greenCircle : redCircle;
          const time = DateTime.fromJSDate(item.time)
            .toLocaleString(DateTime.TIME_WITH_SECONDS);
          return m(
            ListItem,
            {
              title: m("time", `${circle} ${time}`),
              detail: ""
            }
          );
        }),
        !startStop && State.timeLog.slice(0).map((item, index, array) => {
          if (!item.started) return
          const itemTime = DateTime.fromJSDate(item.time)
          const start = itemTime.toLocaleString(DateTime.TIME_WITH_SECONDS);
          let duration;
          let durationLabel;
          let ended = "";
          if (index + 1 < array.length) {
            const nextItem = array[index + 1]
            const nextTime = DateTime.fromJSDate(nextItem.time)
            const end = nextTime.toLocaleString(DateTime.TIME_WITH_SECONDS);
            duration = nextTime.diff(itemTime)
            durationLabel = "took"
            ended = ` | Ended: ${end}`
          } else {
            duration = now.diff(itemTime)
            durationLabel = "taking"
          }
          const note = item.note ? ` - ${item.note}` : ""
          return m(
            ListItem,
            {
              title: m("time", {
                class: durationLabel === "taking" ? "text-primary" : undefined
              }, `${duration.toFormat("hh:mm:ss")}${note}`),
              detail: `Started: ${start}${ended}`
            }
          );
        }).reverse(),
      ]
    )
}

export { TimeLogView }
