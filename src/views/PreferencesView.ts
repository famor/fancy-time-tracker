import m from "mithril";

import PreferencesAirtableView from "./PreferencesAirtableView";
import PreferencesGeneralView from "./PreferencesGeneralView";


const PreferencesView: m.Component = {
  view: () => {
    return m("form.form-horizontal preferences m-2", [
        m(PreferencesGeneralView),
        m(PreferencesAirtableView),
      ]
    );
  }
};

export default PreferencesView;
