import m from "mithril";

import State from "../data/State";
import timezones from "../assets/timezones";
import { ConfirmModal } from "../components/ConfirmModal";


interface TimezoneSelectorAttrs {
  timezone: string;
  onchange: (event: { target: HTMLSelectElement }) => void;
}

const TimezoneSelector: m.Component<TimezoneSelectorAttrs> = {
  view: ({ attrs: { timezone, onchange } }) => {
    if (timezone === "system") timezone = timezones[0]
    return m(".form-group", [
      m(".col-6",
        m("label.form-label", { for: "timezone-input" }, "Timezone")
      ),
      m(".col-6",
        m("select.form-select", {
          id: "timezone-input",
          value: timezone,
          onchange
        }, timezones.map(tz =>
          m("option", { value: tz }, tz)
        ))
      )
    ]);
  }
};

function PreferencesGeneralView(): m.Component {
  let showConfirmDeleteDialog = false

  return {
    view: () => {
      return m("fieldset", [
        m("legend", "General"),
        m(TimezoneSelector, {
          timezone: State.preferences!.timezone,
          onchange: (event: { target: HTMLSelectElement }) => State.setTimezone(timezones[event.target.selectedIndex])
        }),
        m("button.btn btn-error", {
          class: showConfirmDeleteDialog ? "loading" : "",
          disabled: showConfirmDeleteDialog,
          onclick: (event: Event) => {
            event.preventDefault();
            showConfirmDeleteDialog = true;
          }
        }, "Delete data"),
        m(ConfirmModal, {
          active: showConfirmDeleteDialog,
          title: "Delete all data?",
          message: "All data stored in this browser will be deleted.",
          confirmLabel: "Delete",
          confirm: () => {
            State.deleteAll();
          },
          cancel: () => showConfirmDeleteDialog = false
        })
      ]);
    }
  }
}

export default PreferencesGeneralView;
