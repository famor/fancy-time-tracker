import m from "mithril";
import { DateTime, Duration } from "luxon";

import State from "../data/State";
import { Input } from "../components/Input";
import TimeLog from "../data/TimeLog";


declare const DEV: boolean;

function calculateDuration(now: DateTime, timeLog: TimeLog[]) {
  let duration = Duration.fromMillis(0);

  for (let i = 0, start = undefined; i < timeLog.length; i++) {
    const time = DateTime.fromJSDate(timeLog[i].time);
    if (timeLog[i].started) {
      start = start ?? time;
    } else {
      if (start) {
        duration = duration.plus(time.diff(start));
        start = null;
      } else {
        if (DEV) console.log("This shouldn't happen - probably duplicated stop - Removing entry", timeLog[i], start, i);
        continue
      }
    }
    if (i + 1 === timeLog.length && start) {
      duration = duration.plus(now.diff(start));
    }
  }

  return duration;
}

interface TimeAttrs {
  duration: Duration;
}

const Time: m.Component<TimeAttrs> = {
  view: ({ attrs: { duration } }) =>
    m("h1.time text-center", m("time", duration.toFormat("hh : mm : ss")))
};

interface StartStopAttrs {
  start: (note: string) => void;
  stop: () => void;
  started: boolean;
}

function StartStop(): m.Component<StartStopAttrs> {
  let note = "";

  return {
    view: ({ attrs: { start, stop, started } }) =>
      m(".m-2 text_center", [
        m(Input, {
          type: "text",
          value: note,
          placeholder: "Working on ...",
          oninput: (event: { target: HTMLInputElement }) => note = event.target.value,
          maxlength: 280,
          size: 300
        }),
        m(".text-center", [
          m(
            "button.m-2 btn btn-success",
            {
              onclick: () => {
                start(note);
                note = ""
              }
            },
            started ? "Start new" : "Start"
          ),
          started && m(
            "button.m-2 btn btn-primary",
            { onclick: () => stop() },
            "Stop"
          )
        ])
      ])
  };
}

interface TimeViewAttrs {
  now: DateTime
}

const TimeView: m.Component<TimeViewAttrs> = {
  view: ({ attrs: {now} }) => {
    return m(".time", [
      m(Time, { duration: calculateDuration(now, State.timeLog) }),
      m(StartStop, { start: (note) => State.start(note), stop: () => State.stop(), started: State.started() })
    ]);
  }
};

export { TimeView };
