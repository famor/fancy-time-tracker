import m, { RouteDefs } from "mithril";

import State from "./data/State";
import MainPage from "./MainPage";
import { Empty404 } from "./components/Empty";
import pages from "./pages";

import "./app.scss";


declare const DEV: boolean;
if (DEV) console.log("Running in DEV mode!")


window.onerror = function(error) {
  if(DEV) console.log("Global error", error);
  if (error instanceof Event) {
    error.preventDefault();
    error.stopPropagation();
  }
  return true
};
window.addEventListener("unhandledrejection", function(event) {
  if(DEV) console.log("unhandledrejection", event);
  event.preventDefault();
  event.stopPropagation();
});
window.addEventListener("rejectionhandled", function(event) {
  if(DEV) console.log("rejectionhandled", event);
  event.preventDefault();
  event.stopPropagation();
});

let routes: RouteDefs = {}

pages.map(page => routes[page.route] = {
    onmatch: async () => await State.init(),
    render: () => m(MainPage, { page })
  }
)


m.route.prefix = "";
m.route(document.body, "/" + "", {
  ...routes,
  "/:404...": {
    render: () => m(Empty404)
  }
});
