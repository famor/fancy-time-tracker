import path from "path";
import { DefinePlugin } from "webpack";
import type { Configuration } from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import FaviconsWebpackPlugin from "favicons-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import CssMinimizerPlugin from "css-minimizer-webpack-plugin";

const config = (env: { WEBPACK_SERVE: boolean; }, argv: any): Configuration => {
  let extraConfig;
  if (env.WEBPACK_SERVE) {
    extraConfig = {
      devServer: {
        historyApiFallback: true
      }
    };
  }
  return {
    mode: env.WEBPACK_SERVE ? 'development' : 'production',
    ...extraConfig,
    entry: {
      app: "./src/app.ts"
    },
    plugins: [
      new DefinePlugin({ DEV: env.WEBPACK_SERVE }),
      new HtmlWebpackPlugin({
        title: "Time Tracker",
        meta: {
          viewport:
            "user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no"
        }
      }),
      new FaviconsWebpackPlugin({
        logo: path.resolve(__dirname, env.WEBPACK_SERVE ? "icon-dev.svg" : "icon.svg"),
        // https://github.com/itgalaxy/favicons#usage
        favicons: {
          appName: "Fancy Time Tracker",
          appDescription: "Keep track of the time you spend on tasks, and overall daily work time.",
          developerName: "Fancy Apps",
          developerURL: null, // prevent retrieving from the nearest package.json
          // background: "#ddd",
          // theme_color: "#333",
          // display: "standalone",
          // orientation: "any",
          start_url: "/",
          // version: "1",
          logging: false,
          icons: {
            // android: true,
            // appleIcon: true,
            // appleStartup: true,
            favicons: true
            // windows: true,
            // yandex: true,
          }
        }
      }),
      new MiniCssExtractPlugin({
        filename: env.WEBPACK_SERVE ? "[name].css" : "[name].[contenthash].css"
      }),
    ],
    output: {
      filename: "[name].[contenthash].js",
      path: path.resolve(__dirname, "dist"),
      clean: true,
      publicPath: "/"
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          include: path.resolve(__dirname, "src")
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            env.WEBPACK_SERVE ? "style-loader" : MiniCssExtractPlugin.loader,
            "css-loader",
            {
              loader: "sass-loader",
              options: {
                sassOptions: { quietDeps: true }
              }
            }
          ],
          include: path.resolve(__dirname, "src")
        }
      ]
    },
    optimization: {
      minimizer: [new CssMinimizerPlugin(), "..."]
    }
  };
};

export default config;
