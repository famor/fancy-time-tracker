#!/usr/bin/env bash

# Script to update the list of official timezones
# Requires `jq` and `curl`

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

ASSET_DESTINATION="${SCRIPT_PATH}"/../src/assets/timezones.ts

curl -sO https://raw.githubusercontent.com/rogierschouten/tzdata-generate/master/dist/tzdata/timezone-data.json

echo -n "export default " > "${ASSET_DESTINATION}"

# Make an array from the keys of 'zones' object, then pipe again to jq, and prepend that array with "System..."
# https://stackoverflow.com/questions/42245288/add-new-element-to-existing-json-array-with-jq
jq '.zones | keys' timezone-data.json | jq '. |= ["System (Device timezone)"] + .' >> "${ASSET_DESTINATION}"

rm timezone-data.json
